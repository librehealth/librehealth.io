+++
title = "LibreHealth Documentation Team"
+++

The LibreHealth Documentation team maintains the documentation for the community and member projects.

The resources maintained by this team are:

* [LibreHealth EHR Wiki](https://wiki.ehr.librehealth.io) contains the LibreHealth EHR documentation and runs Mediawiki.

* [LibreHealth Community Documentation site](https://docs.librehealth.io) will contain the remainder of the member project documentation. It is running Sphinx with the Read The Docs theme. The repository can be [found here](https://gitlab.com/librehealth/documentation/community-documentation).
